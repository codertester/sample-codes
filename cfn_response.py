# ************************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  License: MIT - See: https://opensource.org/licenses/MIT                 
#
#  cfn_response.py implements CFNModule() () class for:
#   1) send (response) method with Python 3 default  "urllib.request" module instead of 3rd party "requests" module
#   Ref: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-lambda-function-code-cfnresponsemodule.html
#
# ************************************************************************************************************************


from json import dumps
from urllib.parse import urlencode
from urllib.request import Request, urlopen
SUCCESS = "SUCCESS"
FAILED = "FAILED"


class CFNModule():
    """
    A class for sending response, on AWS cloud formation stack during deployment, to custom resources.
    """
      
    def __init__(self):
        pass
    # End  __init__() method

    def send(event, context, responseStatus, responseData, physicalResourceId=None, noEcho=False):
        url = event["ResponseURL"]
        response_body = {}
        response_body["Status"] = responseStatus
        response_body["Reason"] = "{}{}".format("CloudWatch Log Stream: ",  context.log_stream_name)
        response_body['PhysicalResourceId'] = getenv('function_name')
        response_body["StackId"] = event["StackId"]
        response_body["RequestId"] = event["RequestId"]
        response_body["LogicalResourceId"] = event["LogicalResourceId"]
        response_body["NoEcho"] = noEcho
        response_body["Data"] = responseData
        json_response_body = dumps(response_body)
        data = json_response_body.encode('utf-8') # encoded as bytes
        method = "PUT"
        print("Response body:\n" + json_response_body)

        try:
            request = Request(url, data)
            request.get_method = lambda: method
        except(Exception) as put_request_error:
            print("Failed put request execution: ", put_request_error)
    # End  send() method
# End class CFNModule()