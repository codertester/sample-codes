# **************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  License: MIT - See: https://opensource.org/licenses/MIT                 
#
#  secret_manager_credentials.py implements SecretManagerCredentials() class for:
#   1) Obtaining credentials from Secret Manager with boto3 library
#
# ***************************************************************************************

import boto3
import base64
from json import loads
from pprint import pprint
from boto3.session import Session
from botocore.exceptions import ClientError

class SecretManagerCredentials():
    
    """
    A class for obtaining credentials from Secret Manager with boto3 library
    """
      
    def __init__(self, region_name=None, secret_name=None):
        print()
        print("Initiating SecretManagerCredentials Engine...")

    def get_secret(self, access_key=None, secret_key=None, secret_name=None, region_name=None):
        session = Session(aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region_name)
        client = session.client(service_name='secretsmanager', region_name=region_name)
    
        try:
            get_secret_value_response = client.get_secret_value(SecretId=secret_name)
        except(ClientError) as client_error:
            if client_error.response['Error']['Code'] == 'DecryptionFailureException':
                raise client_error
            elif client_error.response['Error']['Code'] == 'InternalServiceErrorException':
                raise client_error
            elif client_error.response['Error']['Code'] == 'InvalidParameterException':
                raise client_error
            elif client_error.response['Error']['Code'] == 'InvalidRequestException':
                raise client_error
            elif client_error.response['Error']['Code'] == 'ResourceNotFoundException':
                raise client_error
            elif client_error.response['Error']['Code'] == 'AccessDeniedException':
                raise client_error
        else:
            if 'SecretString' in get_secret_value_response:
                return get_secret_value_response['SecretString']
            else:
                return base64.b64decode(get_secret_value_response['SecretBinary'])

def main():
    access_key  = "access_key"
    secret_key  = "secret_key"
    secret_name = "secret_name"
    region_name = "region_name"
    smc         = SecretManagerCredentials()
    secret      = loads(smc.get_secret(access_key=access_key, secret_key=secret_key, 
                        secret_name=secret_name, region_name=region_name))
    print("This is the Username: ",  secret.get("username"))
    print("This is the Password: ", secret.get("password"))

# invoke app
if __name__ in ('__main__', 'app'):
    main()