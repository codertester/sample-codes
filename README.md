## SAMPLE CODES

---
This repo contains some  **Python (.py) **, Java (.java), **Terraform (.tf)** and **CloudFormation (.json and .yaml)** sample codes.

---

## Python
1. async_demo.py
2. secret_manager_credentials.py
3. cfn_response.py

## Java 
1. DBClient.java


## Terraform (TF)
1. servers_main.tf


## Cloud Formation (CF)
1. secretManager.json
2. secretManagerParametized.yaml


---