#****************************************************************************
# * @License Starts                                                         *
# *                                                                         *
# * Copyright © 2020 - present. Codertester.  All Rights Reserved.          *
# *                                                                         *
# * License: MIT - See: https://opensource.org/licenses/MIT                 *
# *                                                                         *
# * @License Ends                                                           *
# *                                                                         *
# ***************************************************************************


import asyncio


class AsyncDemo():
    """
    A class that demo python async:
    """

    def __init__(self, gather_length=None, async_length=None):
        print()
        print("Initiating Asyc Demo")
        self.gather_length = gather_length
        self.async_length = async_length
    # End  __init__() method
    
    async def a_method(self):
        # async-compatible action that takes some times to complete
        for index in range(self.async_length):
            print("async-test-baba: ", index)
    # End  a_method() method
	
    def another_method(self):
        # sync action, but it is non-blocking
        print("sync action ....")
	# End  another_method() method
        
    async def do_things(self):
        await self.a_method()
        self.another_method()
        # another_method could also be converted to async, but it is an overkill
        # why? (1) it is non-blocking
        #      (2) if any sync parts of the program called another_method or function,
		#          it would need to be converted to async too
    # End  do_things() method
	
    async def main_async_call(self):
        tasks = []
        for index in range(self.gather_length):
            tasks.append(asyncio.create_task(self.do_things()))
            await asyncio.gather(tasks[index])
	# End  main_async_call() method
# End AsyncDemo() class
    
#testing
def test_async(gather_length=None, async_length=None):
    asm = AsyncDemo(gather_length=gather_length, async_length=async_length)
    main = asm.main_async_call()
    asyncio.run(main)
# End test_async() method

test_async(gather_length=4, async_length=50)