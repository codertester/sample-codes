/*
# ******************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved. 
#
#  License: MIT - See: https://opensource.org/licenses/MIT
#
#  DBClient.java implements DBClient() class for:
#
#    1) Connecting/disconnecting to RDMS databases with JDBC.
#    2) Running queries against databases on the  RDMS
#    Note: a) Assuming Java 8.0 on with Oracle JDBC - J/Connector:
#          b) Oracle Repo of JDBC - J/Connector:
#             https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java_8.0.21-1ubuntu18.04_all.deb
# ********************************************************************************************************************
*/



//util and maths imports
import java.util.List;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collection;
import java.math.BigDecimal;
// db/sql imoports
import java.sql.Time;
import java.sql.Date;
import java.sql.Types;
import java.sql.Timestamp;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSetMetaData;


public class DBClient
{
  
  public void DBClient(){}
  
  public String dbConnectionString(String user, String password, String endpoint, int port, String databaseName, String rdmsName, boolean sslOption)
  {
    String connectionString = null;
    
    if(sslOption == true)
      {
        connectionString = "jdbc:" + rdmsName + "://" + endpoint + ":" + String.valueOf(port) + "/" + databaseName + "?" +  "useSSL=true" + "&user=" + user + "&password=" + password;
      }
      else
      {
        connectionString = "jdbc:" + rdmsName + "://" + endpoint + ":" + String.valueOf(port) + "/" + databaseName + "?" +  "useSSL=false" + "&user=" + user + "&password=" + password;
      }
      return connectionString;
  }
  
  public Connection connectDB(String user, String password, String endpoint, int port, String databaseName, String rdmsName, boolean sslOption)
  {
    Connection connection = null;

    try
    {
      // load db JDBC driver and connect
      String connectionString = dbConnectionString(user, password, endpoint, port, databaseName, rdmsName, sslOption);
      String driverName = "com." + rdmsName + ".jdbc.Driver";
      Class.forName(driverName).newInstance();
      connection = DriverManager.getConnection(connectionString);
      
      if(connection instanceof Connection)
      {
        System.out.println("Successfully connected to database" + databaseName);
      }
    }
    catch(Exception error)
    {
      error.printStackTrace();
      System.out.println("Error: Could not connect to db...");
    }

    return connection;
  }
  
  public void executeQueries(Connection connection, String tableName, String queryString)
  {
    Connection con = connection;
    
    try
    {
      if(con instanceof Connection)
      {
        Statement statement = con.createStatement();
        ResultSet resultSet = statement.executeQuery(queryString);
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        
        // print (view) query results
        System.out.println(queryString);
        int columnsNumber = resultSetMetaData.getColumnCount();
        while (resultSet.next())
        {
          
          for (int index = 1; index <= columnsNumber; index++)
          {
             if (index > 1) System.out.print(",  ");
             String columnValue = resultSet.getString(index);
             System.out.print(columnValue + " " + resultSetMetaData.getColumnName(index));
          }
          System.out.println();
        }
      }
      
    }
    catch(Exception error)
    {
      error.printStackTrace();
      System.out.println();
      System.out.println("Error: Could not query db...");
      System.out.println();
    }
    finally
    {

      if (resultSet != null)
      {
          try
          {
              resultSet.close();
          }
          catch (SQLException sqlError) { } // ignore
  
          resultSet = null;
      }
  
      if (statement != null) {
          try
          {
            statement.close();
          }
          catch (SQLException sqlError) { } // ignore
  
          statement = null;
      }
    
    }

  }

  
  public static void main(String[] args) throws SQLException, ClassNotFoundException
  {
    //instantiate class and define all input and arguement variables for db query
    DBClient dbClient = new DBClient();
    String user = "user";
    String password = "password";
    String endpoint = "localhost";  //localhost, url or ipAddress
    int port = 3306;
    String rdmsName = "mysql";
    String databaseName = "mysql";
    String tableName = "innodb_index_stats";
    boolean sslOption = false;
    String queryString = "SELECT * FROM " + tableName;
    
    //key variables
    String [] keys = dbClient.testKeyVariables();
    
    try
    {
      //1. connect to db
      separator();
      Connection connection = dbClient.connectDB(user, password, endpoint, port, databaseName, sslOption);
   
      //2. query db and confirm keys
      if(connection instanceof Connection)
      {
        //a. query
        System.out.println();
        separator();
        System.out.println("Started query....");
        dbClient.executeQueries(connection, tableName, queryString);
        separator();
        System.out.println("Ended query....");
        separator();
      }
      
      //3. finally, close db connection
      connection.close();
      System.out.println("DB connection closed....");
      separator();
      
    }
    catch(Exception error)
    {
      error.printStackTrace();
      System.out.println();
      System.out.println("Error: Could not query db...");
      System.out.println();
    }
    
  }
}